# About

This lib is used by prayer-timetable-react and future packages for convenient way to keep the prayer calculations separate from the design logic.

## Installation - Yarn:
    yarn add prayer-timetable-lib

## Installation - NPM:
    npm i prayer-timetable-lib

## Usage:
    import { prayersCalc, dayCalc } from 'prayer-timetable-lib'

    prayersCalc( this.state.tomorrow:number, this.state.settings:object, this.state.timetable:object, this.state.jamaahShow:bool, this.state.join:bool, this.state.log:bool )

    dayCalc( this.state.tomorrow:number, { hijrioffset: this.state.hijrioffset:number } )